var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'JS', timeSpent: 11000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var i=0
function buku(durasi, daftarBuku){
    if(daftarBuku[i] != undefined){
        readBooksPromise(durasi, daftarBuku[i])
        .then(function(fullfilled){
            i++
            buku(durasi,books)
        })
        .catch(function(error){})
    }
}
buku(10000,books)