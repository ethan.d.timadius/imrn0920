//soal nomor 1
golden = goldenFunction= () =>{
    console.log("this is golden!!");
}

golden();

console.log('=====================')
//soal nomor 2
const newFunction = function literal(firstName, lastName){
    return {
      firstName,
      lastName,
      fullName() {
        console.log(`${firstName} ${lastName}`)
        return 
      }
    };
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 

  console.log('=====================')
//soal nomor 3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  let{firstName,lastName,destination,occupation} = newObject
  console.log(firstName, lastName, destination, occupation)

console.log('=====================')
  //soal nomor 4
  const west = ["Will", "Chris", "Sam", "Holly"]
  const east = ["Gill", "Brian", "Noel", "Maggie"]
  const combined = [...west,...east]
  //Driver Code
  console.log(combined) 

  console.log('=====================')
  //soal nomor 5
  const planet = "earth"
const view = "glass"
const before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;
 
// Driver Code
console.log(before) 