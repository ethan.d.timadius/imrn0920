var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

arr = []
function arrayToObject(arr) {
    for(i=0; i < arr.length;i++){
    var personObj={
        firstName: arr[i][0],
        lastName:arr[i][1],
        gender:arr[i][2],
        age: (function(){
            let umur= thisYear - arr[i][3]
            if(arr[i][3] == null || umur<0){
                personObj.age = "Invalid Birth Year"
            }else{
                personObj.age = umur
            }
        })
    }
    console.log(i+1 + ". "+personObj.firstName+' '+personObj.lastName+':');
    console.log(personObj)      
    }
}
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
 console.log('======================================================================')
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)

//soal nomor 2
console.log('================')
var keys = {
    0: 'Sepatu Stacattu',
    1:'Baju Zoro',
    2:'Baju H&N',
    3:'Sweater Uniklooh',
    4:'Casing Handphone'
}
var harga = {
    'Sepatu Stacattu':1500000,
    'Baju Zoro':500000,
    'Baju H&N':250000,
    'Sweater Uniklooh':175000,
    'Casing Handphone':50000
}

function shoppingTime(x, y){
    var barang= Object.keys(harga)

    var orang = {
        memberId : x,
        money : y,
        listPurchased : barang,
        changeMoney: function(){
            for(i=0;i<=barang.length;i++){
                this.money -= harga[barang[i]]
                return this.money
            }
        }

    }
    return orang
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
